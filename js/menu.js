var menuState = {

    create: function() {
        // Add a background image
        game.add.image(0, 0, 'menu_background');
        // Display the name of the game
        this.nameLabel = game.add.sprite(game.width/2, 110, 'title');
        this.nameLabel.anchor.setTo(0.5, 0.5);
        this.nameLabel.scale.setTo(0.6, 0.6);
        
        // Show the option
        this.startLabel = game.add.sprite(game.width/2, 370, 'start');
        this.startLabel.anchor.setTo(0.5, 0.5);
        this.startLabel.scale.setTo(0.3, 0.3);
        this.startLabel.inputEnabled = true;
        this.startLabel.events.onInputDown.add(this.start, this);
        this.scoreLabel = game.add.sprite(game.width/2, 430, 'score');
        this.scoreLabel.anchor.setTo(0.5, 0.5);
        this.scoreLabel.scale.setTo(0.3, 0.3);
        this.scoreLabel.inputEnabled = true;
        this.scoreLabel.events.onInputDown.add(this.score, this);
        this.exitLabel = game.add.sprite(game.width/2, 490, 'exit');
        this.exitLabel.anchor.setTo(0.5, 0.5);
        this.exitLabel.scale.setTo(0.3, 0.3);
        this.exitLabel.inputEnabled = true;
        this.exitLabel.events.onInputDown.add(this.exit, this);
    },
    update: function(){
        if(this.startLabel.input.pointerOver()){
            this.startLabel.loadTexture('start_s');
        }else{
            this.startLabel.loadTexture('start');
        }
        if(this.scoreLabel.input.pointerOver()){
            this.scoreLabel.loadTexture('score_s');
        }else{
            this.scoreLabel.loadTexture('score');
        }
        if(this.exitLabel.input.pointerOver()){
            this.exitLabel.loadTexture('exit_s');
        }else{
            this.exitLabel.loadTexture('exit');
        }
    },
    start: function() {
        // Start the actual game
        game.state.start('play');
    },
    score: function() {
        window.location.assign("highscore.html?score=0");
    },
    exit: function() {
        game.destroy();
    }
}; 