var playState = {
    preload: function() {
        this.power = 1;
        this.life = 3;
        this.explode = 1;
        this.center = 6;
        this.count = 0;
        this.isdead = false;
        // Define constants
        this.SHOT_DELAY = 300; // milliseconds (10 bullets/second)
        this.BULLET_SPEED = 500; // pixels/second
        this.NUMBER_OF_BULLETS = 40;
        this.NUMBER_OF_ENEMY1 = 5;
        //enemy
        this.enemy1_time = [1000, 5000, 10000, 13000, 14000, 14500, 15000, 18000, 22000, 23000, 25000, 30000, 40000];
        this.enemy1_x = [100, 200, 300, 250, 300, 450, 50, 200, 50, 400, 210, 300, 450];
        this.enemy2_time = [20000, 45000];
        this.enemy2_x = [250, 400];
        this.enemy2_c = 0;
        this.enemy2_shoot = [20, 150, 250, 350, 450];
        this.enemy2_s = 0;
    }, // The proload state does nothing now.
    create: function() {
        //audio
        this.sound.add('shoot');
        this.sound.add('exe');
        this.sound.add('exp');
        //time
        this.playtime = game.time.now;
        // Removed background color, physics system, and roundPixels.
        // replace 'var score = 0' by global score variable.
        game.global.score = 0;
        //background
        this.stage1 = game.add.sprite(0, -2495, 'stage1');
        //player
        this.fire = game.add.sprite(game.width/2-7, 565, 'fire');
        this.fire.animations.add('rocket', [1, 2], 30, true);
        this.player = game.add.sprite(game.width/2, 550, 'p6');
        this.player.anchor.setTo(0.5, 0.5); // Se the anchor point of player to center.
        this.player.immovable = true;
        game.physics.arcade.enable(this.player);
        //explode
        this.exp1Pool = game.add.group();
        for(var i = 0; i < 10; i++){
            var exp1 = game.add.sprite(100, 100, 'explotion1');
            this.exp1Pool.add(exp1);
            exp1.anchor.setTo(0.5, 0.5);
            exp1.animations.add('abc', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18], 40, false);
            exp1.killOnComplete = true;
            exp1.kill();
        }
        //powerup
        this.powerup = game.add.sprite(game.width/2, game.height/2, 'powerup');
        this.powerup.anchor.setTo(0.5, 0.5);
        this.powerup.scale.setTo(0.7, 0.7);
        game.physics.arcade.enable(this.powerup);
        this.powerup.animations.add('up', [0, 1], 4, true);
        this.powerup.kill();
        //bullet
        this.bulletPool = game.add.group();
        for(var i = 0; i < this.NUMBER_OF_BULLETS; i++) {
            // Create each bullet and add it to the group.
            var bullet = game.add.sprite(0, 0, 'bullet');
            this.bulletPool.add(bullet);
            
            bullet.scale.setTo(0.6, 0.6);
            // Set its pivot point to the center of the bullet
            bullet.anchor.setTo(0.5, 0.5);
    
            // Enable physics on the bullet
            game.physics.arcade.enable(bullet);
    
            // Set its initial state to "dead".
            bullet.kill();
        }
        //bullet_enemy
        this.bullet_enemyPool = game.add.group();
        for(var i = 0; i < this.NUMBER_OF_BULLETS; i++) {
            // Create each bullet and add it to the group.
            var bullet = game.add.sprite(0, 0, 'bullet_enemy');
            this.bullet_enemyPool.add(bullet);
            
            // Set its pivot point to the center of the bullet
            bullet.anchor.setTo(0.5, 0.5);
    
            // Enable physics on the bullet
            game.physics.arcade.enable(bullet);
    
            // Set its initial state to "dead".
            bullet.kill();
        }
        //enemy
        this.enemy1Pool = game.add.group();
        for(var i = 0; i < this.NUMBER_OF_ENEMY1; i++){
            var enemy = game.add.sprite(0, 0, 'enemy1');
            this.enemy1Pool.add(enemy);
            enemy.scale.setTo(0.6, 0.6);
            enemy.anchor.setTo(0.5, 0.5);
            game.physics.arcade.enable(enemy);
            enemy.kill();
        }
        this.enemy2 = game.add.sprite(0, 0, 'enemy2');
        this.enemy2.scale.setTo(0.8, 0.8);
        this.enemy2.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(this.enemy2);
        this.enemy2.kill();
        //life
        this.lifecount1 = game.add.sprite(20, 20, 'p6');
        this.lifecount1.scale.setTo(0.5, 0.5);
        this.lifecount2 = game.add.sprite(43, 20, 'p6');
        this.lifecount2.scale.setTo(0.5, 0.5);
        this.lifecount3 = game.add.sprite(66, 20, 'p6');
        this.lifecount3.scale.setTo(0.5, 0.5);
        // The following part is the same as in previous lecture.
        this.cursor = game.input.keyboard.createCursorKeys();
        this.shoot = game.input.keyboard.addKey(Phaser.Keyboard.Z);
        //emitter
        this.emitter = game.add.emitter(0, 0, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-100, 100);
        this.emitter.setXSpeed(-100, 100);
        this.emitter.setScale(1.5, 0, 1.5, 0, 500);
        //text
        text = game.add.text(game.world.centerX, 25, "score: "+game.global.score, {
            font: "30px Arial",
            fill: "#ff0044",
            align: "center"
        });
        text.strokeThickness = 4;
        text.anchor.setTo(0.5, 0.5);
    },
    update: function() {
        this.player.body.velocity.y = 0;
        this.player.body.velocity.x = 0;
        this.updateText();
        //enemy
        if(game.time.now-this.playtime >= this.enemy1_time[0]){
            this.control_enemy1(this.enemy1_x[0]);
            this.enemy1_time.shift();
            this.enemy1_x.shift();
            
        }
        this.move1();
        if(game.time.now-this.playtime >= this.enemy2_time[0]){
            this.control_enemy2(this.enemy2_x[0]);
            this.enemy2_time.shift();
            this.enemy2_x.shift();
            
        }
        this.move2();
        // Shoot a bullet
        if (this.shoot.isDown){
            this.shootbullet();
        }

        if(this.stage1.y<0){
            this.stage1.y+=0.7;
        }else{
            this.end();
        }
        if(!this.isdead){
            this.movePlayer();
            this.fire.animations.play('rocket');
        }else{
            this.explode_plane();
            this.fire.loadTexture('fire');
        }
         game.physics.arcade.overlap(this.player, this.powerup, this.pup, null, this);
         this.powerup.animations.play('up');
    },
    updateText: function() {
        text.setText("score: " +  game.global.score);
    },
    control_enemy1: function(position_x){
        var enemy = this.enemy1Pool.getFirstDead();
        if (enemy === null || enemy === undefined) return;
        enemy.revive();
        // enemy.checkWorldBounds = true;
        // enemy.outOfBoundsKill = true;
        enemy.reset(position_x, -100);
        enemy.body.velocity.y = 300;
    },
    move1: function(){
        var exp1Pool = this.exp1Pool;
        var exp1 = this.exp1;
        var isdead = this.isdead;
        var player = this.player;
        var playerdead = this.playerdead;
        var the = this;
        var bulletPool = this.bulletPool;
        var shootenemy = this.shootenemy;
        var bullet_enemyPool = this.bullet_enemyPool;
        var sound = this.sound;
        this.enemy1Pool.forEach(function(enemy) {
            if(enemy.alive){
                enemy.body.velocity.y -= 1.8;
                if((enemy.body.velocity.y <= 1&&enemy.body.velocity.y >= -1)||(enemy.body.velocity.y <= -120&&enemy.body.velocity.y >= -122)){
                    shootenemy(enemy.x, enemy.y, player.x, player.y, 200, bullet_enemyPool);
                }
                enemy.rotation = game.physics.arcade.angleBetween(enemy, player)-1.5708;
                bulletPool.forEach(function(bullet){
                    if(game.physics.arcade.overlap(bullet, enemy, null, null, the)){
                        game.global.score+=100;
                        sound.play('exe');
                        exp1(enemy.x, enemy.y, exp1Pool);
                        enemy.kill();
                        bullet.kill();
                    }
                });
                if(game.physics.arcade.overlap(player, enemy, playerdead, null, the)&&!isdead){
                    exp1(enemy.x, enemy.y, exp1Pool);
                    game.global.score+=100;
                    sound.play('exe');
                    enemy.kill();
                }
                if(enemy.y < -140) enemy.kill();
            }
        });
        this.bullet_enemyPool.forEach(function(bullet){
            if(bullet.alive){
                if(game.physics.arcade.overlap(player, bullet, playerdead, null, the)&&!isdead){
                    bullet.kill();
                }
            }
        });

    },
    control_enemy2: function(position_x){
        this.enemy2.revive();
        this.enemy2.reset(position_x, -100);
        this.enemy2.body.velocity.y = 50;
    },
    move2: function(){
        var enemy2 = this.enemy2;
        var c = this.enemy2_c;
        var emitter = this.emitter;
        if(this.enemy2.alive){
            if(this.enemy2.y > 700) {
                this.enemy2.kill();
                this.enemy2_s = 0;
                c=0;
            }
            if(this.enemy2_c >= 9) {
                game.global.score+=300;
                this.exp1(this.enemy2.x, this.enemy2.y, this.exp1Pool);
                this.sound.play('exe');
                this.drop_pup(this.enemy2.x, this.enemy2.y);                
                this.enemy2.kill();
                this.enemy2_s = 0;
                c=0;
            }
            if(game.physics.arcade.overlap(this.player, this.enemy2, this.playerdead, null, this)){
                game.global.score+=300;
                this.exp1(this.enemy2.x, this.enemy2.y, this.exp1Pool);
                this.sound.play('exe');
                this.drop_pup(this.enemy2.x, this.enemy2.y); 
                this.enemy2.kill();
                this.enemy2_s = 0;
                c=0;
            }
            this.bulletPool.forEach(function(bullet){
                if(game.physics.arcade.overlap(bullet, enemy2, null, null, this)){
                    bullet.kill();
                    emitter.x = enemy2.x;
                    emitter.y = enemy2.y;
                    emitter.start(true, 500, null, 15)
                    enemy2.body.velocity.y = 50;
                    c++;
                }
            });
            this.enemy2_c = c;
            //shoot
           if(this.enemy2.y >= this.enemy2_shoot[this.enemy2_s]){
                this.enemy2_s++;
                var trispeed = Math.sqrt((this.player.x-this.enemy2.x)*(this.player.x-this.enemy2.x)+(this.player.y-this.enemy2.y)*(this.player.y-this.enemy2.y));
                var cos = (this.player.x-this.enemy2.x)/trispeed;
                var sin = (this.player.y-this.enemy2.y)/trispeed;
                this.shootenemy(this.enemy2.x, this.enemy2.y, this.player.x, this.player.y, 200, this.bullet_enemyPool);
                this.shootenemy(this.enemy2.x, this.enemy2.y, this.enemy2.x+(this.player.x-this.enemy2.x)/trispeed*10+2*sin, this.enemy2.y+(this.player.y-this.enemy2.y)/trispeed*10+2*cos, 200, this.bullet_enemyPool);
                this.shootenemy(this.enemy2.x, this.enemy2.y, this.enemy2.x+(this.player.x-this.enemy2.x)/trispeed*10-2*sin, this.enemy2.y+(this.player.y-this.enemy2.y)/trispeed*10-2*cos, 200, this.bullet_enemyPool);
           }
        }

    },
    exp1: function(x, y, pool){
        var exp = pool.getFirstDead();
        if (exp === null || exp === undefined) return;
        exp.revive();
        exp.reset(x, y+5);
        exp.animations.play('abc');
    },
    playerdead: function(){
        this.sound.play('exp');
        this.isdead = true;
    },
    movePlayer: function() {
        if (this.cursor.left.isDown){
            if(this.player.x>30){
                this.player.x -= 5;
                this.fire.x -= 5;
            }
            if(this.center > 1){
                if(this.count > 5){
                    this.count = 0;
                    this.center--;
                    this.fire.x--;
                }else{
                    this.count++;
                }
            }
        }else if(this.cursor.right.isDown){
            if(this.player.x<game.width-30){
                this.player.x += 5;
                this.fire.x += 5;
            }
            if(this.center < 11){
                if(this.count > 5){
                    this.count = 0;
                    this.center++;
                    this.fire.x++;
                }else{
                    this.count++;
                }
            }
        }else if(this.cursor.up.isDown&&this.player.y>30){
            this.player.y -= 5;
            this.fire.y -= 5;
        }else if(this.cursor.down.isDown&&this.player.y<game.height-30){
            this.player.y += 5;
            this.fire.y += 5;
        }else{
            if(this.center > 6){
                if(this.count > 9){
                    this.count = 0;
                    this.center--;
                    this.fire.x--;
                }else{
                    this.count++;
                }
            }else if(this.center < 6){
                if(this.count > 9){
                    this.count = 0;
                    this.center++;
                    this.fire.x++;
                }else{
                    this.count++;
                }
            }
        }
        if(this.center<10){
            this.player.loadTexture('p'+String.fromCharCode(this.center+48));
        }else if(this.center == 10){
            this.player.loadTexture('p10');
        }else if(this.center == 11){
            this.player.loadTexture('p11');
        }
    },
    explode_plane: function(){
        if(this.count > 4){
            this.count = 0;
            this.explode++;
        }else{
            this.count++;
        }
        if(this.explode < 10){
            this.player.loadTexture('e'+String.fromCharCode(this.explode+48));
        }else{
            this.life--;
            this.deletelife();
            if(this.life==0){
                this.end();
            }
            //init plane
            this.player.position.x = game.width/2;
            this.player.position.y =  550;
            this.fire.position.x = game.width/2-7;
            this.fire.position.y = 565;
            this.explode = 1;
            this.center = 6;
            this.power = 1;
            this.isdead = false;
        }
    },
    drop_pup: function(x, y){
        this.powerup.revive();
        this.powerup.reset(x, y);
        this.powerup.body.velocity.y = 50;
    },
    pup: function(){
        if(this.power < 3){
            this.power++;
        }
        this.powerup.kill();
    },
    shootbullet: function(){
            // Enforce a short delay between shots by recording
        // the time that each bullet is shot and testing if
        // the amount of time since the last shot is more than
        // the required delay.
        if (this.lastBulletShotAt === undefined) this.lastBulletShotAt = 0;
        if (game.time.now - this.lastBulletShotAt < this.SHOT_DELAY) return;
        this.lastBulletShotAt = game.time.now;
        this.sound.play('shoot');
        // Get a dead bullet from the pool
        var bullet = this.bulletPool.getFirstDead();
        if (bullet === null || bullet === undefined) return;
        bullet.revive();
        // Bullets should kill themselves when they leave the world.
        // Phaser takes care of this for me by setting this flag
        // but you can do it yourself by killing the bullet if
        // its x,y coordinates are outside of the world.
        bullet.checkWorldBounds = true;
        bullet.outOfBoundsKill = true;
        if(this.power >= 2){
            var bullet2 = this.bulletPool.getFirstDead();
            if (bullet2 === null || bullet2 === undefined) return;
            bullet2.revive();
            bullet2.checkWorldBounds = true;
            bullet2.outOfBoundsKill = true;
        }
        if(this.power >= 3){
            var bullet3 = this.bulletPool.getFirstDead();
            if (bullet3 === null || bullet3 === undefined) return;
            bullet3.revive();
            bullet3.checkWorldBounds = true;
            bullet3.outOfBoundsKill = true;
        }

        // Set the bullet position to the gun position.
        if(this.power == 1){
            bullet.reset(this.player.x+this.center-6, this.player.y-10);
            // Shoot it
            bullet.body.velocity.x = 0;
            bullet.body.velocity.y = -this.BULLET_SPEED;
        }else if(this.power == 2){
            bullet.reset(this.player.x+this.center-20, this.player.y-10);
            bullet2.reset(this.player.x+this.center+8, this.player.y-10);
            bullet.body.velocity.x = 0;
            bullet.body.velocity.y = -this.BULLET_SPEED;
            bullet2.body.velocity.x = 0;
            bullet2.body.velocity.y = -this.BULLET_SPEED;
        }else{
            bullet.reset(this.player.x+this.center-6, this.player.y-10);
            bullet2.reset(this.player.x+this.center+12, this.player.y-10);
            bullet2.rotation = 0.1;
            bullet3.reset(this.player.x+this.center-24 , this.player.y-10);
            bullet3.rotation = -0.1;
            bullet.body.velocity.x = 0;
            bullet.body.velocity.y = -this.BULLET_SPEED;
            bullet2.body.velocity.x = Math.sin(bullet2.rotation) * this.BULLET_SPEED;
            bullet2.body.velocity.y = -Math.cos(bullet2.rotation) * this.BULLET_SPEED;
            bullet3.body.velocity.x = Math.sin(bullet3.rotation) * this.BULLET_SPEED;
            bullet3.body.velocity.y = -Math.cos(bullet3.rotation) * this.BULLET_SPEED;
        }
    },
    shootenemy: function(posx, posy, desx, desy, speed, bullet_enemyPool){
        // Get a dead bullet from the pool
        var bullet = bullet_enemyPool.getFirstDead();
        if (bullet === null || bullet === undefined) return;
        bullet.revive();
        // Bullets should kill themselves when they leave the world.
        // Phaser takes care of this for me by setting this flag
        // but you can do it yourself by killing the bullet if
        // its x,y coordinates are outside of the world.
        bullet.checkWorldBounds = true;
        bullet.outOfBoundsKill = true;

        // Set the bullet position to the gun position.
        var trispeed = Math.sqrt((desx-posx)*(desx-posx)+(desy-posy)*(desy-posy));
        bullet.reset(posx, posy);
        bullet.body.velocity.x = speed*(desx-posx)/trispeed;
        bullet.body.velocity.y = speed*(desy-posy)/trispeed;

    },
    deletelife: function(){
        if(this.life==2){
            this.lifecount3.destroy();
        }else if(this.life==1){
            this.lifecount2.destroy();
        }else if(this.life==0){
            this.lifecount1.destroy();
        }
    },
    end: function() {
        // Start the actual game
        var temp = game.global.score;
        game.global.score = 0;
        window.location.assign("highscore.html?score="+temp);
    },
    };