var loadState = {

    preload: function () {
        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width/2, 150,
        'loading...', { font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Display the progress bar
        var progressBar = game.add.sprite(game.width/2, 200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar)
        // Load backfround
        game.load.image('stage1', 'assets/background/stage1.png');
        // Load font
        game.load.image('title', 'assets/title.png');
        game.load.image('start', 'assets/start.png');
        game.load.image('start_s', 'assets/start_s.png');
        game.load.image('score', 'assets/highscore.png');
        game.load.image('score_s', 'assets/highscore_s.png');
        game.load.image('exit', 'assets/exit.png');
        game.load.image('exit_s', 'assets/exit_s.png');
        // Load player
        game.load.image('p1', 'assets/player/p1.png');
        game.load.image('p2', 'assets/player/p2.png');
        game.load.image('p3', 'assets/player/p3.png');
        game.load.image('p4', 'assets/player/p4.png');
        game.load.image('p5', 'assets/player/p5.png');
        game.load.image('p6', 'assets/player/p6.png');
        game.load.image('p7', 'assets/player/p7.png');
        game.load.image('p8', 'assets/player/p8.png');
        game.load.image('p9', 'assets/player/p9.png');
        game.load.image('p10', 'assets/player/p10.png');
        game.load.image('p11', 'assets/player/p11.png');
        game.load.image('e1', 'assets/player/e1.png');
        game.load.image('e2', 'assets/player/e2.png');
        game.load.image('e3', 'assets/player/e3.png');
        game.load.image('e4', 'assets/player/e4.png');
        game.load.image('e5', 'assets/player/e5.png');
        game.load.image('e6', 'assets/player/e6.png');
        game.load.image('e7', 'assets/player/e7.png');
        game.load.image('e8', 'assets/player/e8.png');
        game.load.image('e9', 'assets/player/e9.png');
        game.load.spritesheet('fire', 'assets/player/fire.png', 12, 14);
        game.load.image('bullet', 'assets/player/bullet.png');
        game.load.spritesheet('powerup', 'assets/player/powerup.png', 38, 32);
        //enemy
        game.load.image('enemy1', 'assets/enemy/enemy1.png');
        game.load.image('enemy2', 'assets/enemy/enemy2.png');
        game.load.spritesheet('explotion1', 'assets/explode_1/9.png', 50, 50);
        game.load.image('bullet_enemy', 'assets/enemy/bulletsmall.png');
        game.load.image('pixel', 'assets/pixel.png');
        // Load a new asset that we will use in the menu state
        game.load.image('menu_background', 'assets/background/menu.png');
        //audio
        game.load.audio('exe', 'assets/audio/GSMAEXP.mp3');
        game.load.audio('exp', 'assets/audio/GMIDEXP.mp3');
        game.load.audio('shoot', 'assets/audio/VALCAN.mp3');
        //game.load.audio('aud', 'assets/bodenstaendig_2000_in_rock_4bit.ogg');
    },

    create: function() {
        // Go to the menu state
        game.state.start('menu');
    } 

};