var strUrl = location.search;
var ParaVal;
var score = '';

if (strUrl.indexOf("?") != -1) {
	var getSearch = strUrl.split("?");
	ParaVal = getSearch[1].split("=");
	score=ParaVal[1];
}

function sortFunction(a, b) {
    if (a[0] === b[0]) {
        return 0;
    }
    else {
        return (a[0] > b[0]) ? 1 : -1;
    }
}

function init(){
    var back_btn = document.getElementById('back');
    back_btn.addEventListener('click', function(){
        window.location.assign('index.html');
    })
    var total_sort=[];
    var total_high=[];
    var before = "<div class='row'><div class='col-md-3'></div><div class='col-md-3'><h3>";
    var mid = "</h3></div><div class='col-md-6'><h3>score: "
    var after = "</h3></div></div>";
    var scoreRef = firebase.database().ref('score');
	scoreRef.on('value', function(snapshot){
        total_high=[];
        total_sort=[];
		snapshot.forEach(function (childSnapshot) {
			var value = childSnapshot.val();
            // number++;
            // total_high.push(before+number+'. '+value.name+mid+value.score+after);
            total_sort.push([value.score, value.name]);
        });
        total_sort.sort(sortFunction);
        if(total_sort.length > 10){
            for(var i = 1; i <= 10; i++){
                total_high.push(before+i+'. '+total_sort[i-1][1]+mid+total_sort[i-1][0]+after);
            }
        }else{
            for(var i = 1; i <= total_sort.length; i++){
                total_high.push(before+i+'. '+total_sort[i-1][1]+mid+total_sort[i-1][0]+after);
            }
        }
		document.getElementById('highscore_list').innerHTML = total_high.join(' ');
	});
    if(score != 0){
        document.getElementById('nameinput').innerHTML = '<label for="name" class="sr-only">name</label><input id="name" class="mt-3 mb-5" placeholder="name" required autofocus></input>'
        document.getElementById('score').innerHTML = "<h3>score: "+score+"</h3>";
        document.getElementById('send').innerHTML = "<button class='btn btn-info btn-block' id='sendBtn'>send</button>"
        var send_btn = document.getElementById('sendBtn');
        var name_txt = document.getElementById('name');
        send_btn.addEventListener('click', function(){
            firebase.database().ref('score').push({
            name: name_txt.value,
            score: score
        });
        window.location.assign('index.html');
        })
    }
}

window.onload = function (){
    init();
};