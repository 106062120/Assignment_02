# Software Studio 2019 Spring Midterm Project

# url：https://106062120.gitlab.io/Assignment_02

#Project Name : Raiden

#score:
Complete game process 15%: Yes
Basic rules 20%: Yes
Jucify mechanisms 15%: level: Yes ultimate skills: No
Animations 10%: Yes
Particle Systems 10%: Yes
UI 5%: Yes
Sound effects 5%: Yes
Leaderboard 5%: Yes
Bonus: Enhanced items(3level)

#How to play:
1. click start
2. use 'top' 'down' 'left' 'right' keyboard to move plane
3. use 'z' to attack
4. you can check highscore in highscore